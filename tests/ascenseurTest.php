
<?php


use PHPUnit\Framework\TestCase;

const ETAGEMIN = 0;

const ETAGEMAX = 10;


class ascenseurTest extends TestCase
{

    /*
      test quand on demande d'aller à un étage qui n'existe pas alors il bouge pas
    */
    public function testEtageExist(){
        $this->assertEquals(0, move(0,-2));
    }

    /*
      test quand on demande d'aller à l'etage actuel
    */
    public function testEtageNoMove(){
        $this->assertEquals(0, move(1,1));

    }

    /*
      test quand on demande d'aller à un étage supérieur
    */
    public function testUp(){
      $this->assertEquals(1,move(1,4));

    }

    /*
      test quand on demande d'aller à un étage inférieur
    */
    public function testDown(){
      $this->assertEquals(-1,move(5,1));

    }

    /*
      test si l'etage demandé dans la liste de boutons
    */
    public function testStop(){
      $this->assertTrue(stop(2, [0,1,2,3,4]));
    }

    /*
      test ou aller en fonction de l'etage le plus proche et vers le bas 
    */
    public function testINeedYou(){
      $this->assertEquals(-1, priority(4,[3,5]));
      $this->assertEquals(1, priority(4,[2,5]));
      $this->assertEquals(-1, priority(4,[3,6]));
    }




}

/*
  test si l'etage demandé dans la liste de boutons
*/              
function priority(int $etageActuel, array $buttons){
  $min=ETAGEMAX;
  $value=10;
  foreach($buttons as $button){
    if(abs($button-$etageActuel) < $min){
      if($value>$button){
        $min=abs($button-$etageActuel);
        $value=$button;
      }       
    }
  }
  if($etageActuel>$value){
    return -1;
  }
  else{
    return 1;
  }  


}

/*
  test si on doit monter ou descendre en fonction de la position actuel de l'ascenseur
*/
function move(int $etageActuel, int $etageDemande)
{
  if($etageActuel == $etageDemande || $etageDemande > ETAGEMAX || $etageDemande<ETAGEMIN ){
    return 0;
  }
  if($etageDemande > $etageActuel){
    return 1;
  }
  else{
    return -1;
  }

}

/*
  test si l'etage et dans la liste de boutonss
*/
function stop(int $etage, array $buttons){

  if(in_array($etage, $buttons)){
    return true;
  }
  return false;
}


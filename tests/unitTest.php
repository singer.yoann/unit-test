
<?php


use PHPUnit\Framework\TestCase;

//use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;


class unitTest extends TestCase
{
    /*  Exercice 1A */
    
    /*
    test min to maj
    */
    public function testMinToMaj()
    {
        
        $test = "test";
        $result=strtoupper($test);
        $this->assertEquals("TEST", $result);
    }

    /*
    test maj to maj
    */
    public function testMajIsMaj()
    {
        
        $result="TEST";

        $this->assertEquals("TEST", $result);
    }

    /*
    test min to maj avec des nombres et des maj
    */
    public function testMinToMajWithRandom()
    {
        
        $test = "test6446RERE";
        $result=strtoupper($test);
        $this->assertEquals("TEST6446RERE", $result);
    }


    /*
    test maj to maj avec des minuscule et des nombres
    */
    public function testMajIsMajWithRandom()
    {
        
        $result="TEST8578dhdfbds";

        $this->assertEquals("TEST8578dhdfbds", $result);
    }

    /* Exercice 1B*/

    /*
    test division nombre entier
    */
    public function testDivideInt()
    {
        
        $result=4/2;

        $this->assertIsInt($result);
    }

    public function testDivideFloat()
    {
        
        $result=4.4/2.2;

        $this->assertIsFloat($result);
    }

    public function testDivideFloatInfinite()
    {
        
        $result=2.6/15.87;

        $this->assertEquals($result);
    }

    /*
    test division par zero
    */
    public function testDivideZero()
    {
        $n1=2;
        $n2=0;
        if($n2 == 0){
            throw new InvalidArgumentException('Division par zero.');
        }
        else{
            $result=$n1/$n2;
        }
    }

    /* Exercice 1C */

    /*Vérifier le fonctionnement sur une liste d’entier pour un tri croissant 

Vérifier le fonctionnement sur une liste d’entier pour un tri décroissant 

Vérifier le fonctionnement sur une liste vide 

Vérifier qu’une valeur autre qu’une liste est en erreur 
*/


    /*
    test trie liste croissant
    */
    public function testListCroissant()
    {
        
        $result=[1,4,3,5,2];

        $attendu=[1,2,3,4,5];

        $result=sort($result);
        
        $this->assertTrue(arrays_are_similar($attendu, $result));
    }

    /*
    test trie liste decroissant
    */
    public function testListDecroissant()
    {
        
        $result=[1,4,3,5,2];
        $attendu=[5,4,3,2,1];
        asort($result);

        $this->assertTrue(arrays_are_similar($attendu, $result));
    }



}


function arrays_are_similar($a, $b) {
    // if the indexes don't match, return immediately
    if (count(array_diff_assoc($a, $b))) {
      return false;
    }
    // we know that the indexes, but maybe not values, match.
    // compare the values between the two arrays
    foreach($a as $k => $v) {
      if ($v !== $b[$k]) {
        return false;
      }
    }
    // we have identical indexes, and no unequal values
    return true;
  }